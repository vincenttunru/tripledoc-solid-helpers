# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2019-10-02

### Breaking changes

- Tripledoc and solid-auth-client are no longer direct dependencies, but peerDependencies. This will
  ensure that you are able to install and run your own versions, and having tripledoc-solid-helpers
  pick up that one rather than it having its own copy.

## [0.1.1] - 2019-10-01

### Fixed bugs

- `fetchDocumentForClass` would generate a filename based on the Class's IRI, and although it was
  URLEncoded, the slashes in there would still confuse Node Solid Server. To avoid problems, it's
  now based on either the hash part of the URL, if available, or the deepest path part, and if
  neither is possible, no prefix is generated and the filename is just the date + a random string of
  numbers.

## [0.1.0] - 2019-09-24

### New features

- It is now possible to fetch an arbitrary person's details (using `fetchUser`) rather than just
  those of the person currently logged in (now under `fetchCurrentUser`).

### Breaking changes

- `fetchUser` now requires a WebID as argument. Use `fetchCurrentUser` to retain the old behaviour
  of `fetchUser`.

## [0.0.1] - 2019-09-24

### New features

- First release!
