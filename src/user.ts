import solidAuth from 'solid-auth-client';
import { fetchDocument } from 'tripledoc';

/**
 * Fetch information about the current user.
 *
 * @returns A [TripleSubject](https://vincenttunru.gitlab.io/tripledoc/docs/api/interfaces/triplesubject.html) that can be queried for details about the user, or `null` if the user is not logged in.
 */
export async function fetchCurrentUser() {
  const currentSession = await solidAuth.currentSession();
  if (!currentSession) {
    return null;
  }

  return fetchUser(currentSession.webId);
}

/**
 * Fetch information about a given user.
 *
 * @param webId WebID of the user to fetch
 * @returns A [TripleSubject](https://vincenttunru.gitlab.io/tripledoc/docs/api/interfaces/triplesubject.html) that can be queried for details about the user.
 */
export async function fetchUser(webId: string) {
  const webIdDoc = await fetchDocument(webId);
  const user = webIdDoc.getSubject(webId);

  return user;
}
