import { fetchDocument } from 'tripledoc';
import { solid } from 'rdf-namespaces';
import { fetchCurrentUser } from './user';

/**
 * Fetch the current user's Public Type Index.
 *
 * @returns A [TripleDocument](https://vincenttunru.gitlab.io/tripledoc/docs/api/interfaces/tripledocument.html) that can be queried for invidiual type registrations, or `null` if the user is not logged in or has no references to such a Document.
 */
export async function fetchPublicTypeIndex () {
  const user = await fetchCurrentUser();
  if (user === null) {
    return null;
  }

  const publicTypeIndexUrl = user.getNodeRef(solid.publicTypeIndex);
  if (!publicTypeIndexUrl || typeof publicTypeIndexUrl !== 'string') {
    return null;
  }

  const publicTypeIndex = await fetchDocument(publicTypeIndexUrl);
  return publicTypeIndex;
}
