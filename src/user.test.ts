import { fetchCurrentUser, fetchUser } from './user';

jest.mock('solid-auth-client', () => ({
  currentSession: jest.fn(() => 'https://arbitrary-webid.example'),
}));
jest.mock('tripledoc', () => ({
  fetchDocument: jest.fn(() => ({
    getSubject: jest.fn(() => 'Arbitrary mock subject'),
  })),
}));

describe('fetchCurrentUser', () => {
  it('should return `null` when the user is not logged in', async () => {
    const mockCurrentSession: jest.Mock = require.requireMock('solid-auth-client').currentSession;
    mockCurrentSession.mockReturnValueOnce(undefined);
    expect(await fetchCurrentUser()).toBeNull();
  });

  it('should return the Subject at the current user\'s WebId', async () => {
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;
    const mockGetSubject: jest.Mock = jest.fn(() => 'Some Subject');
    mockFetchDocument.mockReturnValueOnce({ getSubject: mockGetSubject });

    expect(await fetchCurrentUser()).toBe('Some Subject');
  })
});

describe('fetchUser', () => {
  it('should fetch the Document at the given WebId', async () => {
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;

    await fetchUser('https://some-webid.example');

    expect(mockFetchDocument.mock.calls.length).toBe(1);
    expect(mockFetchDocument.mock.calls[0][0]).toBe('https://some-webid.example');
  });

  it('should return the Subject at the given WebId', async () => {
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;
    const mockGetSubject: jest.Mock = jest.fn(() => 'Some Subject');
    mockFetchDocument.mockReturnValueOnce({ getSubject: mockGetSubject });

    expect(await fetchUser('https://some-webid.example')).toBe('Some Subject');
  });
});
