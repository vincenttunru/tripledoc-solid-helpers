import { fetchPublicTypeIndex } from './fetchPublicTypeIndex';

jest.mock('./user');
jest.mock('tripledoc', () => ({
  fetchDocument: jest.fn(() => ({
    getSubject: jest.fn(() => 'Arbitrary mock subject'),
  })),
}));

describe('fetchPublicTypeIndex', () => {
  it('should return `null` when the user is not logged in', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce(null);

    expect(await fetchPublicTypeIndex()).toBeNull();
  });

  it('should return `null` when the profile does not list a public type index', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce({
      getNodeRef: () => null,
    });

    expect(await fetchPublicTypeIndex()).toBeNull();
  });

  it('should return `null` when the profile returns an invalid value for the public type index', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce({
      getNodeRef: () => { some: 'object, if this happens it is a bug in Tripledoc' },
    });

    expect(await fetchPublicTypeIndex()).toBeNull();
  });

  it('should fetch the Document at the location indicated in the user\'s profile', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce({
      getNodeRef: () => 'https://some-public-type-index-url.example',
    });
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;

    await fetchPublicTypeIndex();

    expect(mockFetchDocument.mock.calls.length).toBe(1);
    expect(mockFetchDocument.mock.calls[0][0]).toBe('https://some-public-type-index-url.example');
  });

  it('should return the Document at the location indicated in the user\'s profile', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce({
      getNodeRef: () => 'https://some-public-type-index-url.example',
    });
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;
    mockFetchDocument.mockReturnValueOnce('Some Public Type Index Document')

    expect(await fetchPublicTypeIndex()).toBe('Some Public Type Index Document');
  });
});
