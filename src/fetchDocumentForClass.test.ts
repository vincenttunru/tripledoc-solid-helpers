import { fetchDocumentForClass } from './fetchDocumentForClass';
import { schema, solid, space } from 'rdf-namespaces';
import { NodeRef, createDocument } from 'tripledoc';

const mockSubject = {
  getNodeRef: jest.fn((property: NodeRef) => {
    if (property === solid.instance) {
      return 'https://arbitrary-instance.example/';
    }
    if (property === space.storage) {
      return 'https://arbitrary-storage.example/';
    }
    return 'https://arbitrary-object.example';
  }),
  addNodeRef: jest.fn(),
};
const mockDocument = {
  findSubject: jest.fn(() => mockSubject),
  getSubject: jest.fn(() => mockSubject),
  addSubject: jest.fn(() => mockSubject),
  save: jest.fn(() => Promise.resolve()),
  asNodeRef: jest.fn(() => 'https://arbitrary-document.example'),
};
jest.mock('./user.ts', () => ({
  fetchCurrentUser: jest.fn(() => Promise.resolve(mockSubject)),
}));
jest.mock('./fetchPublicTypeIndex', () => ({
  fetchPublicTypeIndex: jest.fn(() => Promise.resolve(mockDocument)),
}));
jest.mock('tripledoc', () => ({
  createDocument: jest.fn(() => mockDocument),
  fetchDocument: jest.fn(() => Promise.resolve(mockDocument)),
}));

describe('fetchDocumentForClass', () => {
  it('should return `null` when the user is not logged in', async () => {
    const mockFetchCurrentUser: jest.Mock = require.requireMock('./user').fetchCurrentUser;
    mockFetchCurrentUser.mockReturnValueOnce(null);

    expect(await fetchDocumentForClass(schema.Review)).toBeNull();
  });

  it('should return `null` when the user\'s Public Type Index could not be loaded', async () => {
    const mockFetchPublicTypeIndex: jest.Mock =
      require.requireMock('./fetchPublicTypeIndex').fetchPublicTypeIndex;
    mockFetchPublicTypeIndex.mockReturnValueOnce(null);

    expect(await fetchDocumentForClass(schema.Review)).toBeNull();
  });

  it('should return the Document for the given Class if it exists', async () => {
    mockSubject.getNodeRef.mockReturnValueOnce('https://some-document-for-the-class.example')
    const mockFetchDocument: jest.Mock = require.requireMock('tripledoc').fetchDocument;
    mockFetchDocument.mockReturnValueOnce('Some Document');

    const fetchedDocument = await fetchDocumentForClass(schema.Review);

    expect(mockFetchDocument.mock.calls.length).toBe(1);
    expect(mockFetchDocument.mock.calls[0][0]).toBe('https://some-document-for-the-class.example');
    expect(fetchedDocument).toBe('Some Document');
  });

  describe('when no Document exists for the given Class yet', () => {
    it('should try to create a new Document if there is no listing for this Class', async () => {
      // Pretend there is no Subject with solid.forClass === schema.Review:
      mockDocument.findSubject.mockReturnValueOnce(null);

      await fetchDocumentForClass(schema.Review);
  
      // Once to save the new Document, once to update the Public Type Index:
      expect(mockDocument.save.mock.calls.length).toBe(2);
    });

    it('should try to create a new Document if the Class\'s listing in the Type Index does not refer to an instance', async () => {
      // Pretend there is no value for solid.instance:
      mockSubject.getNodeRef.mockReturnValueOnce(null);

      await fetchDocumentForClass(schema.Review);
  
      // Once to save the new Document, once to update the Public Type Index:
      expect(mockDocument.save.mock.calls.length).toBe(2);
    });

    it('should return `null` if it tried to create a Document but no Storage location was found', async () => {
      // Pretend there is no value for solid.instance:
      mockSubject.getNodeRef.mockReturnValueOnce(null);
      // Pretend there is no value for space.storage:
      mockSubject.getNodeRef.mockReturnValueOnce(null);
  
      expect(await fetchDocumentForClass(schema.Review)).toBeNull();
    });

    it('should generate a new Document with a filename based on the Class\'s hash, so those get sorted next to each other', async () => {
      // Pretend there is no Subject with solid.forClass === schema.Review:
      mockDocument.findSubject.mockReturnValueOnce(null);
      const mockCreateDocument: jest.Mock = require.requireMock('tripledoc').createDocument;

      await fetchDocumentForClass('http://www.w3.org/2006/vcard/ns#Address');

      expect(mockCreateDocument.mock.calls.length).toBe(1);
      expect(mockCreateDocument.mock.calls[0][0]).toMatch('/Address');
    });

    it('should generate a new Document with a filename based on the Class\'s path if no hash is specified, so those get sorted next to each other', async () => {
      // Pretend there is no Subject with solid.forClass === schema.Review:
      mockDocument.findSubject.mockReturnValueOnce(null);
      const mockCreateDocument: jest.Mock = require.requireMock('tripledoc').createDocument;

      await fetchDocumentForClass('https://schema.org/Movie');

      expect(mockCreateDocument.mock.calls.length).toBe(1);
      expect(mockCreateDocument.mock.calls[0][0]).toMatch('/Movie');
    });

    it('should generate a new Document with a random filename if the class was a regular URL', async () => {
      // Pretend there is no Subject with solid.forClass === schema.Review:
      mockDocument.findSubject.mockReturnValueOnce(null);
      const mockCreateDocument: jest.Mock = require.requireMock('tripledoc').createDocument;

      await fetchDocumentForClass('https://arbitrary-class.example');

      expect(mockCreateDocument.mock.calls.length).toBe(1);
      expect(mockCreateDocument.mock.calls[0][0]).not.toMatch('arbitrary-class');
    });

    it('should generate a new Document with a random filename if the class was an invalid URL', async () => {
      // Pretend there is no Subject with solid.forClass === schema.Review:
      mockDocument.findSubject.mockReturnValueOnce(null);
      const mockCreateDocument: jest.Mock = require.requireMock('tripledoc').createDocument;

      await fetchDocumentForClass('invalid-url');

      expect(mockCreateDocument.mock.calls.length).toBe(1);
      expect(mockCreateDocument.mock.calls[0][0]).not.toMatch('invalid-url');
    });
  });
});
