import { fetchDocument, NodeRef, TripleDocument, createDocument } from 'tripledoc';
import { solid, space, rdf } from 'rdf-namespaces';

import { fetchCurrentUser } from './user';
import { fetchPublicTypeIndex } from './fetchPublicTypeIndex';

/**
 * Fetch a Document in which Subjects of a certain type are stored.
 *
 * If a Document is listed in the user's Public Type Index for the given class, this function will
 * fetch that Document. If it does not exist yet, it will initialise a new one, add it to the Public
 * Type Index, and return the newly initialised Document.
 *
 * @param rdfClass The type of Subjects stored in this Document.
 * @returns The created Document, or `null` if the Document could not be found or initialised.
 */
export async function fetchDocumentForClass(rdfClass: NodeRef): Promise<TripleDocument | null> {
  const user = await fetchCurrentUser();
  const publicTypeIndex = await fetchPublicTypeIndex();
  if (!user || !publicTypeIndex) {
    return null;
  }

  const classIndex = publicTypeIndex.findSubject(solid.forClass, rdfClass);
  // If a Document containing Subjects of type `rdfClass` exists, return it:
  if (classIndex) {
    const documentRef = classIndex.getNodeRef(solid.instance);
    if (documentRef !== null) {
      const document = await fetchDocument(documentRef);
      return document;
    }
  }

  // No Document for Subjects of type `rdfClass` exists; create it:
  const storage = user.getNodeRef(space.storage);
  if (typeof storage !== 'string') {
    return null;
  }

  // Note: There's an assumption here that `/public/` exists and is writable for this app.
  //       In the future, "Shapes" should hopefully allow us to get more guarantees about this:
  //       https://ruben.verborgh.org/blog/2019/06/17/shaping-linked-data-apps/#need-for-shapes
  const newDocumentRef = storage + 'public/' + generateIdentifierForClass(rdfClass) + '.ttl';
  const newDocument = createDocument(newDocumentRef);
  await newDocument.save();
  const typeRegistration = publicTypeIndex.addSubject();
  typeRegistration.addNodeRef(rdf.type, solid.TypeRegistration);
  typeRegistration.addNodeRef(solid.instance, newDocument.asNodeRef());
  typeRegistration.addNodeRef(solid.forClass, rdfClass);
  await publicTypeIndex.save([typeRegistration]);

  return newDocument;
}

const generateIdentifierForClass = (rdfClass: NodeRef) => {
  let prefix = '';
  try {
    const classUrl = new URL(rdfClass);
    const pathParts = classUrl.pathname.split('/');
    prefix = (classUrl.hash.length > 1) ? classUrl.hash.substring(1) : pathParts[pathParts.length - 1];
  } catch(e) {
    // If `rdfClass` is an invalid URL, or the browser is too old to provide the URL object,
    // that's OK for the name generation — we just do not insert a prefix in front of the file name.
  }

  return prefix + Date.now().toString() + Math.random().toString().substring('0.'.length);
};
